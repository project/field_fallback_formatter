# Fallback Formatter

This module provides a formatter which is able to render a different field if
the actual field is empty.


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

The module has no global configurations. Formatter plugin instances can be 
configured on the Field UI or on the Layout Builder forms.


## Usage

1. Visit the Field UI config form, and select "Formatter with Fallback Field" at the
field where you want to show a fallback.
2. Select the formatter and configure its options for the Main field.
3. Select the fallback field you want to show if the main field is empty.
4. Select and configure the formatter also for the fallback field.
