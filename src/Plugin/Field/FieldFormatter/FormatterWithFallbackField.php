<?php

declare(strict_types=1);

namespace Drupal\field_fallback_formatter\Plugin\Field\FieldFormatter;

use Drupal\Component\Plugin\Factory\DefaultFactory;
use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FieldTypePluginManagerInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FormatterInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of formatter with fallback field.
 *
 * @FieldFormatter(
 *   id = "field_fallback_formatter",
 *   label = @Translation("Formatter with fallback field"),
 *   deriver = "Drupal\field_fallback_formatter\Plugin\Deriver\AllFieldTypeFormatterDeriver"
 * )
 */
class FormatterWithFallbackField extends FormatterBase implements ContainerFactoryPluginInterface {

  const MAIN_FIELD_FORMATTER_ID_KEY = 'main_field_formatter_id';
  const MAIN_FIELD_FORMATTER_SETTINGS_KEY = 'main_field_formatter_settings';
  const FALLBACK_FIELD_ID_KEY = 'fallback_field_id';
  const FALLBACK_FIELD_FORMATTER_ID_KEY = 'fallback_field_formatter_id';
  const FALLBACK_FIELD_FORMATTER_SETTINGS_KEY = 'fallback_field_formatter_settings';

  /**
   * Temporary storage for the fallback field.
   *
   * @var \Drupal\Core\Field\FieldItemListInterface|null
   */
  protected ?FieldItemListInterface $fallbackField = NULL;

  /**
   * Constructs a FormatterBase object.
   *
   * @param string $pluginId
   *   The plugin_id for the formatter.
   * @param mixed $pluginDefinition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $fieldDefinition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $viewMode
   *   The view mode.
   * @param array $thirdPartySettings
   *   Any third party settings.
   * @param \Drupal\Component\Plugin\PluginManagerInterface $formatterPluginManager
   *   The field formatter plugin manager.
   * @param \Drupal\Core\Field\FieldTypePluginManagerInterface $fieldTypeManager
   *   The field type plugin manager.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entityFieldManager
   *   The entity field manager.
   */
  public function __construct(
    string $pluginId,
    $pluginDefinition,
    FieldDefinitionInterface $fieldDefinition,
    array $settings,
    $label,
    string $viewMode,
    array $thirdPartySettings,
    protected PluginManagerInterface $formatterPluginManager,
    protected FieldTypePluginManagerInterface $fieldTypeManager,
    protected EntityFieldManagerInterface $entityFieldManager,
  ) {
    parent::__construct(
      $pluginId,
      $pluginDefinition,
      $fieldDefinition,
      $settings,
      $label,
      $viewMode,
      $thirdPartySettings,
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition,
  ) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('plugin.manager.field.formatter'),
      $container->get('plugin.manager.field.field_type'),
      $container->get('entity_field.manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings(): array {
    return [
      static::MAIN_FIELD_FORMATTER_ID_KEY => NULL,
      static::MAIN_FIELD_FORMATTER_SETTINGS_KEY => [],
      static::FALLBACK_FIELD_ID_KEY => NULL,
      static::FALLBACK_FIELD_FORMATTER_ID_KEY => NULL,
      static::FALLBACK_FIELD_FORMATTER_SETTINGS_KEY => [],
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function view(FieldItemListInterface $items, $langcode = NULL): array {
    $elements = parent::view($items, $langcode);
    $realItems = $this->fallbackField ?? $items;
    $realDefinition = $this->fallbackField?->getFieldDefinition() ?? $this->fieldDefinition;
    $formatter = $this->fallbackField
      ? $this->getSetting(static::FALLBACK_FIELD_FORMATTER_ID_KEY)
      : $this->getSetting(static::MAIN_FIELD_FORMATTER_ID_KEY);

    // Slightly aligned copy of FormatterBase::view(). We try to be as opaque as
    // possible.
    if (Element::children($elements)) {
      $entity = $items->getEntity();
      $info = [
        '#theme' => 'field',
        '#title' => $realDefinition->getLabel(),
        '#label_display' => $this->label,
        '#view_mode' => $this->viewMode,
        '#language' => $realItems->getLangcode(),
        '#field_name' => $realDefinition->getName(),
        '#field_type' => $realDefinition->getType(),
        '#field_translatable' => $realDefinition->isTranslatable(),
        '#entity_type' => $entity->getEntityTypeId(),
        '#bundle' => $entity->bundle(),
        '#object' => $entity,
        '#items' => $realItems,
        '#formatter' => $formatter,
        '#is_multiple' => $realDefinition->getFieldStorageDefinition()->isMultiple(),
        '#third_party_settings' => $this->getThirdPartySettings(),
      ];

      $elements = array_merge($elements, $info);
    }

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode): array {
    $renderable = [];

    if (!($mainFieldFormatterId = $this->getSetting(static::MAIN_FIELD_FORMATTER_ID_KEY))) {
      return $renderable;
    }

    if (!$items->isEmpty()) {
      $mainFormatter = $this->formatterPluginManager->getInstance([
        'field_definition' => $this->fieldDefinition,
        'configuration' => [
          'type' => $mainFieldFormatterId,
          'settings' => $this->getSetting(static::MAIN_FIELD_FORMATTER_SETTINGS_KEY),
          'label' => $this->label,
          'weight' => 0,
        ],
        'view_mode' => $this->viewMode,
      ]);
      $mainFormatter->prepareView([0 => $items]);
      $renderable = $mainFormatter->viewElements($items, $langcode);
    }

    // This part doesn't work with Layout Builder, because when its FieldBlock
    // plugin checks block access, it also involves checking field item list
    // emptiness - and in case of empty fields the access check returns
    // AccessResultForbidden. We have (and need) LayoutBuilderBuildSubscriber to
    // work around that case. But with Field UI, the situation is much easier,
    // we can render the fallback field here.
    if (
      empty($renderable) &&
      ($fallbackFieldId = $this->getSetting(static::FALLBACK_FIELD_ID_KEY)) &&
      ($fallbackFieldFormatterId = $this->getSetting(static::FALLBACK_FIELD_FORMATTER_ID_KEY))
    ) {
      // Save field definition to a temporary property so
      // FormatterInterface::view can add the right field theme metadata.
      $this->fallbackField = $items->getEntity()->hasField($fallbackFieldId)
        ? $items->getEntity()->get($fallbackFieldId)
        : NULL;

      if (
        $this->fallbackField &&
        $this->fallbackField->access('view', NULL, TRUE)->isAllowed() &&
        !$this->fallbackField->isEmpty()
      ) {
        $fallbackFormatter = $this->formatterPluginManager->getInstance([
          'field_definition' => $this->fallbackField->getFieldDefinition(),
          'configuration' => [
            'type' => $fallbackFieldFormatterId,
            'settings' => $this->getSetting(static::FALLBACK_FIELD_FORMATTER_SETTINGS_KEY),
            'label' => $this->label,
            'weight' => 0,
          ],
          'view_mode' => $this->viewMode,
        ]);
        $this->fallbackField->filterEmptyItems();
        $fallbackFormatter->prepareView([0 => $this->fallbackField]);
        $renderable = $fallbackFormatter->viewElements($this->fallbackField, $langcode);
      }
    }

    return $renderable;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary(): array {
    if (
      !($mainFormatterId = $this->getSetting(static::MAIN_FIELD_FORMATTER_ID_KEY)) ||
      !($fallbackField = $this->getSetting(static::FALLBACK_FIELD_ID_KEY)) ||
      !($fallbackFormatterId = $this->getSetting(static::FALLBACK_FIELD_FORMATTER_ID_KEY))
    ) {
      return [$this->t('Not yet configured')];
    }

    $mainFormatter = $this->formatterPluginManager->getInstance([
      'field_definition' => $this->fieldDefinition,
      'configuration' => [
        'type' => $mainFormatterId,
        'settings' => $this->getSetting(static::MAIN_FIELD_FORMATTER_SETTINGS_KEY),
        'label' => '',
      ],
      'view_mode' => '_custom',
    ]);

    $summary[] = $this->t('Main field formatter: %formatter', [
      '%formatter' => $mainFormatter->getPluginDefinition()['label'],
    ]);
    $summary = array_merge(
      $summary,
      (array) $mainFormatter->settingsSummary()
    );

    $fallbackFieldDefinition = $this->getFallbackFieldDefinitions()[$fallbackField];
    $fallbackFormatter = $this->formatterPluginManager->getInstance([
      'field_definition' => $fallbackFieldDefinition,
      'configuration' => [
        'type' => $fallbackFormatterId,
        'settings' => $this->getSetting(static::FALLBACK_FIELD_FORMATTER_SETTINGS_KEY),
        'label' => '',
      ],
      'view_mode' => '_custom',
    ]);

    $summary[] = '---';
    $summary[] = $this->t('Fallback field: %field', [
      '%field' => $fallbackFieldDefinition->getLabel(),
    ]);
    $summary[] = $this->t('Fallback field formatter: %formatter', [
      '%formatter' => $fallbackFormatter->getPluginDefinition()['label'],
    ]);
    return array_merge(
      $summary,
      (array) $fallbackFormatter->settingsSummary()
    );
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state): array {
    $form = parent::settingsForm($form, $form_state);

    // Formatter configurations for the current field (applied if field is not
    // empty).
    $form += $this->getFormatterElementsForField(
      $this->fieldDefinition,
      'main_field_formatter',
      $form_state,
      $this->getSettings(),
      $this->t('Main field'),
    );

    // Visual separator between main and fallback field formatter configs.
    $form[] = [
      '#type' => 'html_tag',
      '#tag' => 'hr',
    ];

    // The fallback field and its formatter configuration. First, select the
    // fallback field.
    $availableFallbackFieldDefinitions = $this->getFallbackFieldDefinitions();
    $fallbackFieldId = $this->getValuesFromFormState($form_state, static::FALLBACK_FIELD_ID_KEY);
    $fallbackFieldFormOptions = array_map(
      fn ($definition) => $definition->getLabel() ?: $definition->getName(),
      $availableFallbackFieldDefinitions
    );
    $form[static::FALLBACK_FIELD_ID_KEY] = [
      '#type' => 'select',
      '#title' => $this->t('Fallback field'),
      '#options' => $fallbackFieldFormOptions,
      '#default_value' => $fallbackFieldId ?: NULL,
      '#ajax' => [
        'callback' => [__CLASS__, 'onFallbackFieldNameChange'],
      ],
      '#required' => TRUE,
    ];

    $form += $this->getFormatterElementsForField(
      $availableFallbackFieldDefinitions[$fallbackFieldId] ?? NULL,
      'fallback_field_formatter',
      $form_state,
      $this->getSettings(),
      $this->t('Fallback field'),
    );

    return $form;
  }

  /**
   * Returns formatter plugin select and formatter settings elements of a field.
   *
   * @param \Drupal\Core\Field\FieldDefinitionInterface|null $fieldDefinition
   *   The field definition of the field. Method returns only placeholder
   *   containers with the appropriate HTML IDs if NULL.
   * @param string $formElementsKeyBase
   *   Base string to use as the base of the formatter ID select element and the
   *   parent key of formatter settings elements.
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   The form state object of the Field UI / Layout Builder block configure
   *   form.
   * @param array $displayOptions
   *   The display options array.
   * @param string|\Drupal\Core\StringTranslation\TranslatableMarkup $fieldLabelContext
   *   Label context to use in the label of the formatter ID select element.
   *
   * @return array[]
   *   Form elements with the formatter ID select element and the form elements
   *   of the selected formatter.
   */
  protected function getFormatterElementsForField(
    ?FieldDefinitionInterface $fieldDefinition,
    string $formElementsKeyBase,
    FormStateInterface $formState,
    array $displayOptions,
    string|TranslatableMarkup $fieldLabelContext,
  ): array {
    $formatterIdKey = "{$formElementsKeyBase}_id";
    $formatterSettingsKey = "{$formElementsKeyBase}_settings";
    $formatterIdAjaxId = Html::getId($formatterIdKey) . '-ajax';
    $formatterSettingsAjaxId = Html::getId($formatterSettingsKey) . '-ajax';

    // No field definition, return only the placeholders required for AJAX form
    // update.
    if (!$fieldDefinition) {
      return [
        $formatterIdKey => [
          '#type' => 'container',
          '#optional' => FALSE,
          '#id' => $formatterIdAjaxId,
        ],
        $formatterSettingsKey => [
          '#type' => 'container',
          '#optional' => FALSE,
          '#id' => Html::getId($formatterSettingsKey) . '-ajax',
        ],
      ];
    }

    // Build the formatter plugin ID select element.
    $formatters = $this->getApplicablePluginOptions($fieldDefinition);
    $currentFormatterId = $this->getValuesFromFormState($formState, $formatterIdKey);
    if (
      !$currentFormatterId ||
      !array_key_exists($currentFormatterId, $formatters)
    ) {
      $currentFormatterId = $this->getDefaultFormatterId($fieldDefinition->getType());
    }
    $form[$formatterIdKey] = [
      '#type' => 'select',
      '#title' => $this->t('Plugin for @title', [
        '@title' => $fieldLabelContext,
      ]),
      '#options' => $formatters,
      '#default_value' => $currentFormatterId,
      '#ajax' => [
        'callback' => [__CLASS__, 'onFormatterTypeChange'],
        'wrapper' => $formatterSettingsAjaxId,
        'event' => 'change',
        'method' => 'replaceWith',
      ],
      '#prefix' => "<div id=\"$formatterIdAjaxId\">",
      '#suffix' => '</div>',
      '#required' => TRUE,
    ];

    // Elements of the formatter settings form.
    $formatter = $this->formatterPluginManager->getInstance([
      'field_definition' => $fieldDefinition,
      'configuration' => [
        'type' => $currentFormatterId,
        'settings' => $displayOptions[$formatterSettingsKey] ?? [],
        'label' => '',
        'weight' => 0,
      ],
      'view_mode' => '_custom',
    ]);
    $form[$formatterSettingsKey] = $formatter->settingsForm([], $formState)
      ?: ['#value' => []];
    $form[$formatterSettingsKey]['#prefix'] = "<div id=\"$formatterSettingsAjaxId\">";
    $form[$formatterSettingsKey]['#suffix'] = '</div>';

    return $form;
  }

  /**
   * Return formatter value from form state (falling back to current settings).
   *
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   The form state object.
   * @param string|array $keys
   *   Keys representing levels in form structure, starting from this field
   *   formatter's "settings" element.
   *
   * @return mixed
   *   The value from form state object, or if it isn't present there, from the
   *   current settings property.
   */
  protected function getValuesFromFormState(
    FormStateInterface $formState,
    string|array $keys,
  ): mixed {
    $path = [
      'fields',
      $this->fieldDefinition->getName(),
      'settings_edit_form',
      'settings',
      ... (array) $keys,
    ];

    if ($formState->hasValue($path)) {
      return $formState->getValue($path);
    }

    // Fall back to saved settings.
    if (!$this->defaultSettingsMerged) {
      $this->mergeDefaults();
    }
    $valuesToReturn = $this->settings;

    foreach ((array) $keys as $key) {
      $valuesToReturn = $valuesToReturn[$key] ?? NULL;
      if ($valuesToReturn === NULL) {
        return NULL;
      }
    }
    return $valuesToReturn;
  }

  /**
   * Returns an array of applicable formatter options for a field.
   *
   * @param \Drupal\Core\Field\FieldDefinitionInterface $fieldDefinition
   *   The field definition.
   *
   * @return array
   *   An array of applicable formatter plugin labels, keyed by their plugin ID.
   */
  protected function getApplicablePluginOptions(FieldDefinitionInterface $fieldDefinition): array {
    $options = $this->formatterPluginManager->getOptions($fieldDefinition->getType());
    // Exclude this type.
    unset($options[$this->getPluginId()]);
    $applicable_options = [];
    foreach ($options as $option => $label) {
      $plugin_class = DefaultFactory::getPluginClass($option, $this->formatterPluginManager->getDefinition($option));
      if (
        is_callable([$plugin_class, 'isApplicable']) &&
        $plugin_class::isApplicable($fieldDefinition)
      ) {
        $applicable_options[$option] = $label;
      }
    }
    return $applicable_options;
  }

  /**
   * Returns the plugin ID of the given field type's default formatter.
   *
   * @param string $fieldType
   *   The type of the field.
   *
   * @return string|null
   *   Plugin ID of the field type's default formatter. Might be empty if the
   *   field type has no default formatter (e.g. because it does not have UI).
   */
  protected function getDefaultFormatterId(string $fieldType): ?string {
    return $this->fieldTypeManager->getDefinitions()[$fieldType]['default_formatter'] ?? NULL;
  }

  /**
   * Returns the list of the applicable fallback field definitions.
   *
   * @return \Drupal\Core\Field\FieldDefinitionInterface[]
   *   List of the applicable fallback field definitions. The current field and
   *   fields not being display configurable are excluded.
   */
  protected function getFallbackFieldDefinitions(): array {
    $field_defs = $this->entityFieldManager->getFieldDefinitions(
      $this->fieldDefinition->getTargetEntityTypeId(),
      $this->fieldDefinition->getTargetBundle()
    );
    // Remove the current field from the list.
    unset($field_defs[$this->fieldDefinition->getName()]);

    // Remove fields which are marked as non-configurable.
    return array_filter(
      $field_defs,
      fn (FieldDefinitionInterface $definition): bool => $definition->isDisplayConfigurable('view')
    );
  }

  /**
   * Ajax callback updating formatter subform if fallback field is changed.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   The form state.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   Ajax commands updating the formatter form (Field UI / Layout Builder form
   *   substructure).
   */
  public static function onFallbackFieldNameChange(
    array $form,
    FormStateInterface $formState,
  ): AjaxResponse {
    $triggeringElement = $formState->getTriggeringElement();

    $response = new AjaxResponse();

    if (!empty($triggeringElement['#array_parents'])) {
      $parents = $triggeringElement['#array_parents'];
      // Remove the triggering fallback field select element.
      array_pop($parents);

      // Replace the formatter plugin ID select element.
      $response->addCommand(
        new ReplaceCommand(
          '#fallback-field-formatter-id-ajax',
          NestedArray::getValue(
            $form,
            array_merge($parents, [static::FALLBACK_FIELD_FORMATTER_ID_KEY])
          )
        )
      );

      // Replace the formatter plugin settings elements.
      $response->addCommand(
        new ReplaceCommand(
          '#fallback-field-formatter-settings-ajax',
          NestedArray::getValue(
            $form,
            array_merge($parents, [static::FALLBACK_FIELD_FORMATTER_SETTINGS_KEY])
          )
        )
      );
    }

    return $response;
  }

  /**
   * Ajax callback updating formatter subform if a formatter field is changed.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   The form state.
   *
   * @return array
   *   Replacement renderable array of the formatter settings form elements.
   */
  public static function onFormatterTypeChange(
    array $form,
    FormStateInterface $formState,
  ): array {
    $triggeringElement = $formState->getTriggeringElement();

    if (!empty($triggeringElement['#array_parents'])) {
      $parents = $triggeringElement['#array_parents'];
      // Remove the triggering element and add the appropriate "*_settings" key
      // instead.
      $plugin_id_key = array_pop($parents);
      $parents[] = preg_replace('/^(.*)_id$/', '$1_settings', $plugin_id_key);

      return NestedArray::getValue($form, $parents);
    }

    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies(): array {
    $dependenciesToMerge = [
      $this->getMainFormatterDependencies(),
      $this->getFallbackFormatterDependencies(),
    ];
    $dependencies = parent::calculateDependencies();
    foreach ($dependenciesToMerge as $dependencySet) {
      $dependencies = NestedArray::mergeDeep($dependencies, $dependencySet);
    }
    return $dependencies;
  }

  /**
   * Returns the dependencies of the formatter used on the main field.
   *
   * @return array
   *   The calculated formatter dependencies.
   *
   * @see \Drupal\Component\Plugin\DependentPluginInterface::calculateDependencies()
   */
  protected function getMainFormatterDependencies(): array {
    $formatter = $this->formatterPluginManager->getInstance([
      'field_definition' => $this->fieldDefinition,
      'configuration' => [
        'type' => $this->getSetting(static::MAIN_FIELD_FORMATTER_ID_KEY),
        'settings' => $this->getSetting(static::MAIN_FIELD_FORMATTER_SETTINGS_KEY) ?? [],
        'label' => '',
        'weight' => 0,
      ],
      'view_mode' => '_custom',
    ]);
    assert($formatter instanceof FormatterInterface);

    return static::pullFormatterDependencies($formatter);
  }

  /**
   * Returns the dependencies of the formatter used on the fallback field.
   *
   * @return array
   *   The calculated formatter dependencies.
   *
   * @see \Drupal\Component\Plugin\DependentPluginInterface::calculateDependencies()
   */
  protected function getFallbackFormatterDependencies(): array {
    $fallbackFieldName = $this->getSetting(static::FALLBACK_FIELD_ID_KEY);
    if (!$fallbackFieldName) {
      return [];
    }
    $fallbackFieldDefinition = $this->getFallbackFieldDefinitions()[$fallbackFieldName] ?? NULL;
    if (!$fallbackFieldDefinition) {
      return [];
    }

    $fallbackFormatter = $this->formatterPluginManager->getInstance([
      'field_definition' => $fallbackFieldDefinition,
      'configuration' => [
        'type' => $this->getSetting(static::FALLBACK_FIELD_FORMATTER_ID_KEY),
        'settings' => $this->getSetting(static::FALLBACK_FIELD_FORMATTER_SETTINGS_KEY) ?? [],
        'label' => '',
        'weight' => 0,
      ],
      'view_mode' => '_custom',
    ]);
    assert($fallbackFormatter instanceof FormatterInterface);

    return static::pullFormatterDependencies($fallbackFormatter);
  }

  /**
   * Returns the dependencies of a formatter instance.
   *
   * @param \Drupal\Core\Field\FormatterInterface $formatter
   *   The formatter to pull dependencies from.
   *
   * @return array
   *   The calculated formatter dependencies.
   *
   * @see \Drupal\Component\Plugin\DependentPluginInterface::calculateDependencies()
   */
  protected static function pullFormatterDependencies(FormatterInterface $formatter): array {
    return array_merge(
      ['module' => [$formatter->getPluginDefinition()['provider'] ?? 'core']],
      $formatter->calculateDependencies(),
    );
  }

}
