<?php

declare(strict_types=1);

namespace Drupal\field_fallback_formatter\EventSubscriber;

use Drupal\Core\Session\AccountInterface;
use Drupal\field_fallback_formatter\Plugin\Field\FieldFormatter\FormatterWithFallbackField;
use Drupal\layout_builder\Event\SectionComponentBuildRenderArrayEvent;
use Drupal\layout_builder\LayoutBuilderEvents;
use Drupal\layout_builder\Plugin\Block\FieldBlock;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Layout Builder render subscriber for field blocks.
 *
 * This class is responsible for doing the necessary workaround for Layout
 * Builder's FieldBlock plugins which are configured to use the
 * "fallback_formatter" field formatter. This field formatter renders a fallback
 * field with the configured formatter if the original field is empty.
 *
 * Unfortunately, the approach what the field formatter uses does not work for
 * Layout Builder, because when the FieldBlock plugin checks block access, it
 * also involves checking field item list emptiness - and in case of empty
 * fields, the access check returns AccessResultForbidden. We have (and need)
 * this event subscriber to work around that case.
 *
 * @see \Drupal\layout_builder\Plugin\Block\FieldBlock::blockAccess()
 * @see \Drupal\field_fallback_formatter\Plugin\Field\FieldFormatter\FormatterWithFallbackField::viewElements()
 */
class LayoutBuilderBuildSubscriber implements EventSubscriberInterface {

  /**
   * Constructs a new LayoutBuilderBuildSubscriber instance.
   *
   * @param \Drupal\Core\Session\AccountInterface $currentUser
   *   The current user service.
   */
  public function __construct(
    protected AccountInterface $currentUser,
  ) {}

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    // We need higher priority than
    // \Drupal\layout_builder\EventSubscriber\BlockComponentRenderArray.
    $events[LayoutBuilderEvents::SECTION_COMPONENT_BUILD_RENDER_ARRAY] = [
      'onBuildRender',
      150,
    ];
    return $events;
  }

  /**
   * Applies the needed workaround for field blocks using fallback formatter.
   *
   * Switches the rendered field and formatter settings for FieldBlock plugin
   * instances using "fallback_formatter" formatter if the original block is not
   * accessible by the current user.
   *
   * @param \Drupal\layout_builder\Event\SectionComponentBuildRenderArrayEvent $event
   *   The section component render event.
   */
  public function onBuildRender(SectionComponentBuildRenderArrayEvent $event) {
    $block = $event->getPlugin();
    if (!$block instanceof FieldBlock) {
      return;
    }

    // Only check access if the component is not being previewed.
    if ($event->inPreview()) {
      return;
    }

    // If current user has access to the actual block plugin instance, no need
    // to do anything.
    if ($block->access($this->currentUser)) {
      return;
    }

    $blockConfig = $block->getConfiguration();
    $fieldFormatterConfig = $blockConfig['formatter'] ?? [];

    // If the formatter ID config isn't set to "field_fallback_formatter",
    // we don't touch the block instance.
    if (
      empty($fieldFormatterConfig['type']) ||
      $fieldFormatterConfig['type'] !== 'field_fallback_formatter'
    ) {
      return;
    }

    // Do we miss a mandatory config?
    if (empty($fallbackField = $fieldFormatterConfig['settings'][FormatterWithFallbackField::FALLBACK_FIELD_ID_KEY])) {
      return;
    }

    $blockReflection = new \ReflectionClass($block);
    $fieldName = $blockReflection->getProperty('fieldName');
    $fieldName->setValue($block, $fallbackField);

    $blockFallbackConfig = $blockConfig;
    $blockFallbackConfig['formatter']['type'] = $fieldFormatterConfig['settings'][FormatterWithFallbackField::FALLBACK_FIELD_FORMATTER_ID_KEY];
    $blockFallbackConfig['formatter']['settings'] = $fieldFormatterConfig['settings'][FormatterWithFallbackField::FALLBACK_FIELD_FORMATTER_SETTINGS_KEY] ?? [];
    $block->setConfiguration($blockFallbackConfig);
  }

}
