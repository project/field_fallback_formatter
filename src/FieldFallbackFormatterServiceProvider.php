<?php

declare(strict_types=1);

namespace Drupal\field_fallback_formatter;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderInterface;
use Drupal\field_fallback_formatter\EventSubscriber\LayoutBuilderBuildSubscriber;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Registers Layout Builder render subscriber if Layout Builder is installed.
 */
class FieldFallbackFormatterServiceProvider implements ServiceProviderInterface {

  /**
   * {@inheritdoc}
   */
  public function register(ContainerBuilder $container): void {
    // Goal here is to prevent making Layout Builder a hard dependency.
    $modules = $container->getParameter('container.modules');
    if (!isset($modules['layout_builder'])) {
      return;
    }

    $container
      ->register(
      'fallback_field.layout_builder.render_subscriber',
      LayoutBuilderBuildSubscriber::class
      )
      ->addArgument(new Reference('current_user'))
      ->addTag('event_subscriber');
  }

}
