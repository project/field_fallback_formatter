<?php

declare(strict_types=1);

namespace Drupal\Tests\field_fallback_formatter\FunctionalJavascript;

use Behat\Mink\Element\NodeElement;
use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\field_fallback_formatter\Plugin\Field\FieldFormatter\FormatterWithFallbackField;
use Drupal\FunctionalJavascriptTests\WebDriverTestBase;
use Drupal\Tests\field_fallback_formatter\Traits\FallbackFieldTestSetupTrait;

/**
 * Tests the formatter settings form, especially the AJAX magic.
 *
 * @group field_fallback_formatter
 */
class FieldFallbackFormatterSettingsTest extends WebDriverTestBase {

  use FallbackFieldTestSetupTrait;

  protected const ENTITY_TYPE = 'user';
  protected const ENTITY_BUNDLE = 'user';
  protected const MAIN_FIELD = 'field_test_main';
  protected const MAIN_FIELD_TYPE = 'text_long';
  protected const FALLBACK_FIELD = 'field_test_fallback';
  protected const FALLBACK_FIELD_TYPE = 'string_long';
  protected const EMPTY_FIELD = 'field_test_empty';
  protected const EMPTY_FIELD_TYPE = 'string';

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'field_fallback_formatter',
    'field_ui',
    'text',
  ];

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $this->setupFields();
    $this->drupalLogin($this->rootUser);
  }

  /**
   * Tests the formatter settings form and its AJAX operations with Field UI.
   *
   * @param string $buttonToClick
   *   The button to click at the end of the configuration.
   *
   * @dataProvider providerTestWithFieldUi
   */
  public function testWithFieldUi(string $buttonToClick): void {
    $this->drupalGet('/admin/config/people/accounts/display');

    // Drag main field row to be visible.
    $assertSession = $this->assertSession();
    $contentRegion = $assertSession->waitForElement('css', 'tr.region-content-message:not(.region-populated), tr.region-content-message.region-populated + tr');
    $this->assertInstanceOf(NodeElement::class, $contentRegion);
    $mainFieldDataSelector = '[data-drupal-selector="edit-fields-' . str_replace('_', '-', static::MAIN_FIELD) . '"]';
    $mainFieldRowInHiddenRegion = $assertSession->waitForElement('css', $hiddenMainRowSelector = "tr.region-hidden-title ~ tr$mainFieldDataSelector");
    $this->assertInstanceOf(NodeElement::class, $mainFieldRowInHiddenRegion);
    $mainFieldRowInHiddenRegion->find('css', '.handle')->dragTo($contentRegion);
    $assertSession->assertWaitOnAjaxRequest();
    $assertSession->assertNoElementAfterWait('css', $hiddenMainRowSelector);

    // Select "Field Fallback formatter".
    $mainFieldRow = $assertSession->waitForElement('css', $mainFieldRowCss = "tr$mainFieldDataSelector");
    $assertSession->waitForElement('css', '[name="fields[' . static::MAIN_FIELD . '][type]"]')->selectOption('field_fallback_formatter');
    $assertSession->assertWaitOnAjaxRequest();
    // Formatter is not yet configured.
    $assertSession->waitForText('Not yet configured');

    // Open formatter settings subform.
    $mainFieldRow->find('css', '[name="' . static::MAIN_FIELD . '_settings_edit"]')->click();
    $assertSession->assertWaitOnAjaxRequest();

    // Configure formatter of the main field.
    $assertSession->waitForField('Plugin for Main field')->selectOption('text_trimmed');
    $assertSession->waitForField('Trimmed limit')->setValue('122');

    // Select the fallback field.
    $mainFieldRow->findField('fields[' . static::MAIN_FIELD . '][settings_edit_form][settings][fallback_field_id]')->selectOption(static::EMPTY_FIELD);
    $assertSession->assertWaitOnAjaxRequest();

    // Configure the formatter of the fallback field.
    $assertSession->waitForField('Plugin for Fallback field')->selectOption('string');
    $assertSession->fieldExists('Link to the User')->click();
    $assertSession->buttonExists($buttonToClick)->click();

    // Save the form. If we click 'Save', then there won't be any AJAX callback.
    if (!str_contains($mainFieldRow->getText(), '---')) {
      // But otherwise, we should wait until AJAX finishes.
      $assertSession->assertWaitOnAjaxRequest();
    }

    // Settings summary should appear.
    $assertSession->elementTextContains('css', $mainFieldRowCss, 'Main field formatter: Trimmed');
    $assertSession->elementTextContains('css', $mainFieldRowCss, 'Trimmed limit: 122 characters');
    $assertSession->elementTextContains('css', $mainFieldRowCss, '---');
    $assertSession->elementTextContains('css', $mainFieldRowCss, 'Fallback field: Empty field label');
    $assertSession->elementTextContains('css', $mainFieldRowCss, 'Fallback field formatter: Plain text');
    $assertSession->elementTextContains('css', $mainFieldRowCss, 'Linked to the User');

    // After we save the view display, the settings of the field should appear
    // in the component settings.
    $assertSession->buttonExists('Save')->click();
    $viewDisplay = \Drupal::service('entity_display.repository')->getViewDisplay(
      static::ENTITY_TYPE,
      static::ENTITY_BUNDLE,
    );
    $this->assertInstanceOf(EntityViewDisplayInterface::class, $viewDisplay);
    $this->assertSame(
      [
        'type' => 'field_fallback_formatter',
        'label' => 'above',
        'settings' => [
          FormatterWithFallbackField::MAIN_FIELD_FORMATTER_ID_KEY => 'text_trimmed',
          FormatterWithFallbackField::MAIN_FIELD_FORMATTER_SETTINGS_KEY => [
            'trim_length' => 122,
          ],
          FormatterWithFallbackField::FALLBACK_FIELD_ID_KEY => static::EMPTY_FIELD,
          FormatterWithFallbackField::FALLBACK_FIELD_FORMATTER_ID_KEY => 'string',
          FormatterWithFallbackField::FALLBACK_FIELD_FORMATTER_SETTINGS_KEY => [
            'link_to_entity' => TRUE,
          ],
        ],
        'third_party_settings' => [],
        'weight' => 0,
        'region' => 'content',
      ],
      $viewDisplay->getComponent(static::MAIN_FIELD)
    );
    $fieldIdPrefix = 'field.field.' . static::ENTITY_TYPE . '.' . static::ENTITY_BUNDLE;
    $this->assertSame(
      [
        'config' => [
          $fieldIdPrefix . '.' . static::EMPTY_FIELD,
          $fieldIdPrefix . '.' . static::FALLBACK_FIELD,
          $fieldIdPrefix . '.' . static::MAIN_FIELD,
        ],
        'module' => [
          'field_fallback_formatter',
          'text',
          'user',
        ],
      ],
      $viewDisplay->getDependencies()
    );
  }

  /**
   * Data provider for ::testWithFieldUi.
   *
   * @return array[]
   *   The test cases.
   */
  public function providerTestWithFieldUi(): array {
    return [
      'Update' => [
        'button to click' => 'Update',
      ],
      'Save' => [
        'button to click' => 'Save',
      ],
    ];
  }

  /**
   * Tests the settings form and its AJAX operations with Layout Builder.
   */
  public function testWithLayoutBuilder(): void {
    \Drupal::service('module_installer')->install(['layout_builder']);
    $this->drupalGet('/admin/config/people/accounts/display');

    $assertSession = $this->assertSession();
    // Enable Layout Builder.
    $assertSession->fieldExists('Use Layout Builder')->click();
    $assertSession->buttonExists('Save')->click();
    // Customize.
    $assertSession->waitForLink('Manage layout')->click();
    $assertSession->waitForLink('Add block')->click();
    $assertSession->assertWaitOnAjaxRequest();
    $assertSession->waitForLink('Main field label')->click();
    $assertSession->assertWaitOnAjaxRequest();
    $assertSession->waitForField('Formatter')->selectOption('field_fallback_formatter');
    $assertSession->assertWaitOnAjaxRequest();

    // Configure formatter of the main field.
    $assertSession->waitForField('Plugin for Main field')->selectOption('text_trimmed');
    $assertSession->assertWaitOnAjaxRequest();
    $assertSession->waitForField('Trimmed limit')->setValue('22');

    // Select the fallback field.
    $assertSession->waitForField('Fallback field')->selectOption(static::EMPTY_FIELD);
    $assertSession->assertWaitOnAjaxRequest();

    // Configure fallback field formatter.
    $assertSession->waitForField('Plugin for Fallback field')->selectOption('string');
    $assertSession->fieldExists('Link to the User')->click();
    $assertSession->fieldExists('User')->selectOption('layout_builder.entity');
    $assertSession->buttonExists('Add block')->click();
    $assertSession->assertWaitOnAjaxRequest();

    // Label of the main field should appear.
    $this->assertTrue($assertSession->waitForText('Main field label'));
  }

}
