<?php

declare(strict_types=1);

namespace Drupal\Tests\field_fallback_formatter\Kernel;

use Drupal\Component\Utility\DeprecationHelper;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\KernelTests\Core\Entity\EntityKernelTestBase;
use Drupal\layout_builder\Entity\LayoutBuilderEntityViewDisplay;
use Drupal\layout_builder\Section;
use Drupal\layout_builder\SectionComponent;
use Drupal\Tests\field_fallback_formatter\Traits\FallbackFieldTestSetupTrait;
use Drupal\user\Entity\Role;
use Drupal\user\RoleInterface;

/**
 * Verifies field and formatter fallback functionality.
 *
 * @group field_fallback_formatter
 */
class FieldFallbackFormatterTest extends EntityKernelTestBase {

  use FallbackFieldTestSetupTrait;

  protected const ENTITY_TYPE = 'entity_test';
  protected const ENTITY_BUNDLE = 'entity_test';
  protected const MAIN_FIELD = 'field_test';
  protected const MAIN_FIELD_TYPE = 'text_long';
  protected const FALLBACK_FIELD = 'field_test_fallback';
  protected const FALLBACK_FIELD_TYPE = 'string_long';
  protected const EMPTY_FIELD = 'field_test_empty';
  protected const EMPTY_FIELD_TYPE = 'string';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'block',
    'contextual',
    'field_fallback_formatter',
    'layout_builder',
    'layout_discovery',
  ];

  /**
   * The test entity.
   *
   * @var \Drupal\Core\Entity\ContentEntityInterface
   */
  protected ContentEntityInterface $entity;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installConfig(['filter']);

    // Use Stark theme for testing markup output.
    \Drupal::service('theme_installer')->install(['stark']);
    $this->config('system.theme')->set('default', 'stark')->save();
    $this->installEntitySchema(static::ENTITY_TYPE);

    // Grant the 'view test entity' permission.
    $this->installConfig(['user']);
    Role::load(RoleInterface::ANONYMOUS_ID)
      ->grantPermission('view test entity')
      ->save();

    $this->setupFields();

    $entityTypeManager = \Drupal::entityTypeManager();
    $this->entity = $entityTypeManager->getStorage(static::ENTITY_TYPE)
      ->create([
        'name' => $this->randomMachineName(),
        static::MAIN_FIELD => [
          [
            'value' => 'Main field value',
            'format' => 'plain_text',
          ],
        ],
        static::FALLBACK_FIELD => 'Fallback field value',
      ]);
    if ($bundleKey = $entityTypeManager->getDefinition(static::ENTITY_TYPE)->getKey('bundle')) {
      $this->entity->set($bundleKey, static::ENTITY_BUNDLE);
    }
    $this->entity->save();
  }

  /**
   * Assert inaccessible items don't change the data of the fields.
   *
   * @param string $mainField
   *   Name of the main field to test with.
   * @param array $formatterSettings
   *   Settings of the fallback formatter.
   * @param string $expected
   *   The expected rendered output of the formatter.
   *
   * @dataProvider providerTest
   */
  public function testFallbackFormatter(
    string $mainField,
    array $formatterSettings,
    string $expected,
  ): void {
    $renderable = $this->entity->get($mainField)->view([
      'type' => 'field_fallback_formatter',
      'settings' => $formatterSettings,
    ]);
    $rendered = DeprecationHelper::backwardsCompatibleCall(
      currentVersion: \Drupal::VERSION,
      deprecatedVersion: '10.3',
      currentCallable: fn() => \Drupal::service('renderer')->renderInIsolation($renderable),
      deprecatedCallable: fn() => \Drupal::service('renderer')->renderPlain($renderable),
    );

    $this->assertEquals($expected, trim((string) $rendered));
  }

  /**
   * Tests the merging of cache metadata.
   *
   * @param string $mainField
   *   Name of the main field to test with.
   * @param array $formatterSettings
   *   Settings of the fallback formatter.
   * @param string $expected
   *   The expected rendered output of the formatter.
   *
   * @dataProvider providerTest
   */
  public function testFallbackFormatterWithLayoutBuilder(
    string $mainField,
    array $formatterSettings,
    string $expected,
  ): void {
    $sectionComponent = new SectionComponent(
      $uuid = \Drupal::service('uuid')->generate(),
      'content',
      [
        'id' => implode(':', [
          'field_block',
          static::ENTITY_TYPE,
          static::ENTITY_BUNDLE,
          $mainField,
        ]),
        'label_display' => 0,
        'context_mapping' => [
          'entity' => 'layout_builder.entity',
        ],
        'formatter' => [
          'type' => 'field_fallback_formatter',
          'label' => 'visible',
          'settings' => $formatterSettings,
        ],
      ],
    );

    LayoutBuilderEntityViewDisplay::create([
      'targetEntityType' => static::ENTITY_TYPE,
      'bundle' => static::ENTITY_BUNDLE,
      'mode' => 'default',
      'status' => TRUE,
    ])
      ->enableLayoutBuilder()
      ->setThirdPartySetting(
        'layout_builder',
        'sections',
        [
          new Section(
            'layout_onecol',
            [],
            [$uuid => $sectionComponent]
          ),
        ]
      )
      ->save();

    $entityRenderable = \Drupal::entityTypeManager()
      ->getViewBuilder(static::ENTITY_TYPE)
      ->view($this->entity, 'default');

    $renderedEntity = \Drupal::service('renderer')->renderPlain($entityRenderable);
    $this->assertStringContainsString($expected, (string) $renderedEntity);
  }

  /**
   * Data provider of test methods.
   *
   * @return array[]
   *   The test cases.
   */
  public function providerTest(): array {
    return [
      'No fallback' => [
        'field' => static::MAIN_FIELD,
        'settings' => [
          'main_field_formatter_id' => 'basic_string',
          'main_field_formatter_settings' => [],
          'fallback_field_id' => static::FALLBACK_FIELD,
          'fallback_field_formatter_id' => 'string',
          'fallback_field_formatter_settings' => ['link_to_entity' => FALSE],
        ],
        'expected' => <<<MARKUP
<div>
    <div>Main field label</div>
              <div><p>Main field value</p>
</div>
          </div>
MARKUP
        ,
      ],
      'Fallback' => [
        'field' => static::EMPTY_FIELD,
        'settings' => [
          'main_field_formatter_id' => 'basic_string',
          'main_field_formatter_settings' => [],
          'fallback_field_id' => static::FALLBACK_FIELD,
          'fallback_field_formatter_id' => 'string',
          'fallback_field_formatter_settings' => ['link_to_entity' => FALSE],
        ],
        'expected' => <<<MARKUP
<div>
    <div>Fallback field label</div>
              <div>Fallback field value</div>
          </div>
MARKUP
        ,
      ],
    ];
  }

}
