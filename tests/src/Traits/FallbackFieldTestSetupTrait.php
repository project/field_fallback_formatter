<?php

declare(strict_types=1);

namespace Drupal\Tests\field_fallback_formatter\Traits;

use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;

/**
 * Trait for setting up test fields.
 */
trait FallbackFieldTestSetupTrait {

  /**
   * Creates three test fields based on class constants.
   */
  protected function setupFields(): void {
    // Set up test fields.
    FieldStorageConfig::create([
      'field_name' => static::MAIN_FIELD,
      'entity_type' => static::ENTITY_TYPE,
      'type' => static::MAIN_FIELD_TYPE,
      'settings' => [],
    ])->save();
    FieldStorageConfig::create([
      'field_name' => static::FALLBACK_FIELD,
      'entity_type' => static::ENTITY_TYPE,
      'type' => static::FALLBACK_FIELD_TYPE,
      'settings' => [],
    ])->save();
    FieldStorageConfig::create([
      'field_name' => static::EMPTY_FIELD,
      'entity_type' => static::ENTITY_TYPE,
      'type' => static::EMPTY_FIELD_TYPE,
      'settings' => [],
    ])->save();
    FieldConfig::create([
      'entity_type' => static::ENTITY_TYPE,
      'bundle' => static::ENTITY_BUNDLE,
      'field_name' => static::MAIN_FIELD,
      'label' => 'Main field label',
    ])->save();
    FieldConfig::create([
      'entity_type' => static::ENTITY_TYPE,
      'bundle' => static::ENTITY_BUNDLE,
      'field_name' => static::FALLBACK_FIELD,
      'label' => 'Fallback field label',
    ])->save();
    FieldConfig::create([
      'entity_type' => static::ENTITY_TYPE,
      'bundle' => static::ENTITY_BUNDLE,
      'field_name' => static::EMPTY_FIELD,
      'label' => 'Empty field label',
    ])->save();
  }

}
